package net.nnemesis.forest;

/**
 * Created by Nicholas on 2015-10-28.
 */
public class Command {
    private String raw;

    private String[] args;
    private int cmd;

    public Command(String raw) {
        this.raw = raw;

        String[] split = raw.split(" ");
        args = new String[split.length - 1];

        for (int i = 1; i < split.length; i++) {
            args[i - 1] = split[i];
        }

        cmd = getCommandID(split[0]);
    }

    public int getCmd() {
        return cmd;
    }

    public String getArg(int i) {
        return args[i];
    }

    public static int getCommandID(String cmd) {
        switch (cmd.toLowerCase()) {
            case "stop":
                return SYS_STOP;
            case "disc":
                return NET_DISCONNECT;
            default:
                return UNKNOWN;
        }
    }

    public static final int UNKNOWN = 0;
    public static final int SYS_STOP = 1;
    public static final int NET_DISCONNECT = 2;
}
