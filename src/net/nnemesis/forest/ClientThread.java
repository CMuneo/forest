package net.nnemesis.forest;

import java.net.Socket;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.util.ArrayList;

public class ClientThread extends Thread implements Client {
    private boolean running = true;

    private Socket s;
    private BufferedReader in;
    private PrintWriter out;

    private ArrayList<String> messageList;
    private ArrayList<InputListener> inputListeners;

    private int id;

    public ClientThread(Socket s, int id) throws Exception {
        this.s = s;

        in = new BufferedReader(new InputStreamReader(s.getInputStream()));
        out = new PrintWriter(s.getOutputStream(), true);

        messageList = new ArrayList<String>();

        System.out.println("(Net) Client connected with ip " + s.getInetAddress().toString());
    }

    public void run() {
        try {
            while (running) {
                while (messageList.size() != 0) {
                    out.print(messageList.remove(0));
                }
            }

            in.close();
            out.close();

            s.close();

            System.out.println("(Net) Client disconnected with ip " + s.getInetAddress().toString());
        } catch (Exception e) {
            System.out.println("(Net) Client error, ip: " + s.getInetAddress().toString() + ", error: " + e.toString());
        }
    }

    public void sendMessage(String msg, boolean important) {
        if (important) {
            out.print(msg); // VERY bad for thread safety, only use when really important.
        } else {
            messageList.add(msg);
        }
    }

    public void addInputListener(InputListener list) {
        inputListeners.add(list);
    }

    public void disconnect(boolean force) {
        if (!force) {
            try {
                while (messageList.size() != 0) {
                    out.print(messageList.remove(0));
                }
            } catch (Exception e) { }
        }

        running = false;
    }

    public int getID() {
        return id;
    }
}
